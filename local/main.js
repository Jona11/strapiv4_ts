// Example API request using fetch
const login = async (identifier, password) => {
  await fetch('http://localhost:1337/api/auth/local', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      identifier,
      password,
    }),
  })
    .then(async(response) => {
      console.log(await response.json())
    })
    .then((data) => {
      // Handle API response here
    })
    .catch((error) => {
      // Handle error here
    })
}
